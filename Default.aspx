﻿<%@  Page Title="" Language="C#" MasterPageFile="~/Citizen.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Main" ContentPlaceHolderID="Content" runat="Server">
    <section id="mainColumn">
        <h2>Välkommen att fylla i din anmälan av miljöbrott</h2>
        <p class="info">I Småstad är inga brott för små för att anmälas</p>

        <p>Fyll i formuläret nedan, alla rutor markerade med stjärna (*) måste fyllas i.</p>

        <p>Inskriven information sparas tillfälligt i en cookie.</p>

        <div id="actualForm">
            <p>
                <span class="label">Var har brottet skett någonstans?</span><br />
                <asp:TextBox ID="PlaceTextBox" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="PlaceFieldValidator" runat="server" ControlToValidate="PlaceTextBox" Display="Dynamic" CssClass="error" ErrorMessage="Detta fält måste fyllas i. "></asp:RequiredFieldValidator>
                *
            </p>
            <p>
                <span class="label">Vilken typ av brott?</span><br />
                <asp:TextBox ID="CrimeTextBox" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="CrimeFieldValidator" runat="server" ControlToValidate="CrimeTextBox" Display="Dynamic" CssClass="error" ErrorMessage="Detta fält måste fyllas i. "></asp:RequiredFieldValidator>
                *
            </p>
            <p>
                <span class="label">När skedde brottet?</span><br />
                <asp:TextBox ID="CrimeDateTextBox" runat="server" ReadOnly="True"></asp:TextBox>
                <ajaxToolkit:CalendarExtender ID="CrimeDateCalendarExtender" runat="server" 
                    TargetControlID="CrimeDateTextBox" 
                    Format="yyyy-MM-dd"
                    />
                <asp:RequiredFieldValidator  ID="CrimeDateRequiredFieldValidator" runat="server" ControlToValidate="CrimeDateTextBox" Display="Dynamic" CssClass="error" ErrorMessage="Detta fält måste fyllas i. "></asp:RequiredFieldValidator>
                *
            </p>
            <p>
                <span class="label">Ditt namn (för- och efternamn):</span><br />
                <asp:TextBox ID="InformersNameTextBox" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="InformersNameFieldValidator" runat="server" ControlToValidate="InformersNameTextBox" Display="Dynamic" CssClass="error" ErrorMessage="Detta fält måste fyllas i. "></asp:RequiredFieldValidator>
                *
            </p>
            <p>
                <span class="label">Din telefon:</span><br />
                <asp:TextBox ID="InformersPhoneTextBox" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="InformersPhoneRequiredFieldValidator" runat="server" ControlToValidate="InformersPhoneTextBox" Display="Dynamic" CssClass="error" ErrorMessage="Detta fält måste fyllas i. "></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="InformersPhoneRegexFieldValidator" runat="server" ControlToValidate="InformersPhoneTextBox" 
                    Display="Dynamic" CssClass="error"
                    ValidationExpression="^\d{2,4}-\d{5,9}"
                    ErrorMessage="Skriv ett giltigt telefonnummer. ">
                </asp:RegularExpressionValidator>
                *
            </p>
            <p>
                <span class="label">Beskriv din observation<br />
                    (ex. namn på misstänkt person):</span><br />
                <asp:TextBox ID="ObservationTextBox" runat="server" TextMode="MultiLine" Rows="5" Columns="16"></asp:TextBox>
            </p>
            <p>
                <asp:Button ID="SendButton" CssClass="button" runat="server" Text="Skicka In"  OnClick="SendButton_Click"/>
            </p>
        </div>
    </section>
    <!-- End Left -->
</asp:Content>

