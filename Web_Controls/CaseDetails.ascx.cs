﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Represents the detailed view of the current Case.
/// </summary>
public partial class CaseDetails : System.Web.UI.UserControl
{
    protected Case _CurrentCase;
    protected List<string> _Samples;
    protected List<string> _Pictures;
    private string _StoreUrl;

    protected void Page_Load(object sender, EventArgs e)
    {
        _CurrentCase = (Case)HttpContext.Current.Items["Case"];
        _Samples = (List<string>)HttpContext.Current.Items["Samples"];
        _Pictures = (List<string>)HttpContext.Current.Items["Pictures"];
        _StoreUrl = (string)HttpContext.Current.Items["StoreUrl"];

        this.DataBind();

        populateSamples();
        populatePictures();
    }

    // List of samples to display
    private void populateSamples()
    { 
        SampleRepeater.DataSource = _Samples
            .ConvertAll(s => new { Name = s, Url = _StoreUrl + s });
        SampleRepeater.DataBind();
    }

    // list of pictures to display
    private void populatePictures()
    {
        PictureRepeater.DataSource = _Pictures
            .ConvertAll(s => new { Name = s, Url = _StoreUrl + s });
        PictureRepeater.DataBind();
    }

}