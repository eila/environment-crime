﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CaseDetails.ascx.cs" Inherits="CaseDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<section id="leftColumn">

    <h3>Anmälan</h3>
    <p>
        <span class="label">Typ av brott:</span><br />
        <%# _CurrentCase.TypeOfCrime %>
    </p>
    <p>
        <span class="label">Brottsplats: </span>
        <br />
        <%# _CurrentCase.Place %>
    </p>
    <p>
        <span class="label">Brottsdatum: </span>
        <br />
        <%# _CurrentCase.DateOfObservation %>
    </p>
    <p>
        <span class="label">Anmälare: </span>
        <br />
        <%# _CurrentCase.InformerName %>
    </p>
    <p>
        <span class="label">Telefon: </span>
        <br />
        <%# _CurrentCase.InformerPhone %>
    </p>
    <p>
        <span class="label">Observationer:</span><br />
        <%# _CurrentCase.Observation %>
    </p>
</section>

<section id="rightColumn">
    <h3>Utredning</h3>
    <p>
        <span class="label">Status:</span><br />
        <%# _CurrentCase.StatusName %>
    </p>
    <p>
        <span class="label">Ansvarig avdelning: </span>
        <br />
        <%# _CurrentCase.DepartmentName %>
    </p>
    <p>
        <span class="label">Handläggare: </span>
        <br />
        <%# _CurrentCase.EmployeeName %>
    </p>
    <p>
        <span class="label">Provtagning: </span>
        <br />
        <asp:Repeater ID="SampleRepeater" runat="server">
            <ItemTemplate>
                <a href="<%# Eval("Url") %>"><%# Eval("Name") %></a><br />
            </ItemTemplate>
        </asp:Repeater>
    </p>
    <p>
        <span class="label">Ytterligare information: </span>
        <br />
        <%# _CurrentCase.Info %>
    </p>
    <p>
        <span class="label">Händelser: </span>
        <br />
        <%# _CurrentCase.Action %>
    </p>
    <p>
        <span class="label">Bilder: </span>
        <br />
        <asp:Label ID="ExtendButton" CssClass="button" runat="server" Text="Visa Bilder" Width="90%" />
        <asp:Panel ID="ExtendPanel" runat="server">
            <ajaxToolkit:CollapsiblePanelExtender
                ID="MapCollapsiblePanelExtender"
                TargetControlID="ExtendPanel"
                ExpandControlID="ExtendButton"
                CollapseControlID="ExtendButton"
                ExpandedText="Dölj Bilder"
                CollapsedText="Visa Bilder"
                TextLabelID="ExtendButton"
                runat="server"
                Collapsed="True" />
            <asp:Repeater ID="PictureRepeater" runat="server">
                <ItemTemplate>
                    <a href="<%# Eval("Url") %>"><%# Eval("Name") %></a><br />
                </ItemTemplate>
            </asp:Repeater>

        </asp:Panel>
    </p>
</section>
