﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Citizen.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <section id="mainColumn">
        <h2>Inloggning för Småstads personal</h2>
        <p class="info">En säker tjänst från Småstads IT-avdelning</p>
        <p>
            <span class="label">Användarnamn:</span><br />
            <asp:TextBox ID="UserNameTextBox" runat="server" />
            <asp:RequiredFieldValidator ID="UserNameValidator" runat="server" ControlToValidate="UserNameTextBox" Display="Dynamic" CssClass="error" ErrorMessage="Detta fält måste fyllas i. " />
        </p>
        <p>
            <span class="label">Lösenord:</span><br />
            <asp:TextBox ID="PasswordTextBox" TextMode="Password" runat="server" />
            <asp:RequiredFieldValidator ID="PasswordValidator" runat="server" ControlToValidate="PasswordTextBox" Display="Dynamic" CssClass="error" ErrorMessage="Detta fält måste fyllas i. " />
        </p>
        <p>
            <asp:Label ID="VerificationLabel" Visible="false" CssClass="error" Text="Fel uppgifter" runat="server" />
        </p>
        <p>
            <asp:Button ID="LoginButton" CssClass="button" runat="server" Text="Logga in" OnClick="LoginButton_Click" /><br />
        </p>
    </section>
    <!-- End Left -->
</asp:Content>

