﻿<%@ Page Title="" Language="C#" MasterPageFile="~/investigator/Investigator.master" AutoEventWireup="true" CodeFile="CrimeInvestigator.aspx.cs" Inherits="investigator_CrimeInvestigator" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Web_Controls/CaseDetails.ascx" TagPrefix="uc1" TagName="CaseDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">

    <h2>Detaljer för ärende: <%# _CurrentCase.ID %></h2>
    <p class="info">Du är inloggad som handläggare</p>

    <div id="map">
        <p class="error">Det ska vara en karta här. Är javascript aktiverat?</p>
    </div>

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDfy0hZbkGoDGgmJitd8CGd5nNG60OZzBE"></script>
    <script type="text/javascript" src="../js/Map.js"></script>

    <input type="hidden" id="LongitudeHiddenField" value="" runat="server" />
    <input type="hidden" id="LatitudeHiddenField" value="" runat="server" />

    <uc1:CaseDetails runat="server" ID="CaseDetails"></uc1:CaseDetails>

    <section id="bottomColumn">
        <h3>Redigera ärende</h3>
        <p class="label">Händelser:</p>
        <asp:TextBox ID="ActionTextBox" runat="server" TextMode="MultiLine" Rows="5" Columns="60"></asp:TextBox>

        <p class="label">Mer information:</p>
        <asp:TextBox ID="InfoTextBox" runat="server" TextMode="MultiLine" Rows="5" Columns="60"></asp:TextBox>

        <p class="label">Prover:</p>
        <asp:FileUpload ID="SampleFileUpload" runat="server" />

        <p class="label">Ladda upp bilder:</p>
        <asp:FileUpload ID="PictureFileUpload" runat="server" />

        <p class="label">Ändring av status:</p>
            <ajaxToolkit:ComboBox ID="StatusComboBox" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"></ajaxToolkit:ComboBox>

        <p>
            <asp:Button ID="SaveButton" CssClass="button" runat="server" OnClick="SaveButton_Click" Text="Spara ändringarna" />
            <asp:Image Visible="false" ImageUrl="~/images/Loading_icon.gif" Height="100" ID="LoadingImage"  runat="server" />
        </p>
    </section>
</asp:Content>

