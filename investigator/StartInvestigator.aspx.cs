﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class investigator_StartInvestigator : System.Web.UI.Page
{
    private SQLBackEnd _BackEnd = new SQLBackEnd();
    string statusFilter, idFilter;
    Employee _User;

    protected void Page_Load(object sender, EventArgs e)
    {
        statusFilter = Request.QueryString["sid"];
        idFilter = Request.QueryString["id"];

        _User = _BackEnd.GetEmployee(User.Identity.Name);

        if (!IsPostBack)
        {
            //Populates the Status Dropdown list
            var statusList = _BackEnd.GetStatusList().ToList();
            statusList.Insert(0, _BackEnd.AllObject);
            PopulateDropDownList(StatusDropDown, statusList);

            StatusDropDown.SelectedValue = statusFilter;
            CaseIdSearchBox.Text = idFilter;
        }

        PopulateCaseList();
    }

    /// <summary>
    /// Populates a given dropdownlist with given values. the provided values 
    /// must be a list with KeyValuePairs. Key the dropdowns data field and 
    /// value the text of each element.
    /// </summary>
    /// <param name="list">dropdownlist to populate</param>
    /// <param name="data">to fill the dropdownlist with</param>
    private void PopulateDropDownList(DropDownList list, List<KeyValuePair<string, string>> data)
    {
        list.DataSource = data;
        list.DataTextField = "Value";
        list.DataValueField = "Key";
        list.DataBind();
    }

    /// <summary>
    /// Populates the cases on the main screen.
    /// </summary>
    private void PopulateCaseList()
    {
        CaseList.DataSource = _BackEnd.GetCaseList()
            // The investigator can only sees his departments cases
            .Where(c => c.EId == _User.ID)
            // The users filter
            .Where(c => statusFilter == null || c.SId == statusFilter || statusFilter == _BackEnd.AllObject.Key)
            .Where(c => idFilter == null || c.ID.Contains(idFilter));
        CaseList.DataBind();
    }

    protected void OtherFilterButton_Click(object sender, EventArgs e)
    {
        statusFilter = StatusDropDown.SelectedValue;
        UpdateList();
    }

    protected void IdFilterButton_Click(object sender, EventArgs e)
    {
        idFilter = CaseIdSearchBox.Text;
        UpdateList();
    }

    /// <summary>
    /// Refreshes the page with the selected filters.
    /// </summary>
    private void UpdateList()
    {
        Response.Redirect("StartInvestigator.aspx?sid=" + statusFilter + "&id=" + idFilter);
    }
}