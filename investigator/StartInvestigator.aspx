﻿<%@ Page Title="" Language="C#" MasterPageFile="Investigator.master" AutoEventWireup="true" CodeFile="StartInvestigator.aspx.cs" Inherits="investigator_StartInvestigator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">


    <h2>Ärenden</h2>
    <p class="info">Du är inloggad som handläggare</p>
    <table id="managerForm">
        <tr>
            <td class="label">Välj status:</td>
            <td>&nbsp;</td>
            <td class="label">Ärendenummer:</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="StatusDropDown" runat="server"></asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="OtherFilterButton" CssClass="button" Text="Hämta lista" OnClick="OtherFilterButton_Click" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="CaseIdSearchBox" runat="server"></asp:TextBox>
            </td>
            <td>                
                <asp:Button ID="IdFilterButton" CssClass="button" Text="Hämta lista" OnClick="IdFilterButton_Click" runat="server" />
            </td>
        </tr>
    </table>

    <!--Nedan ser man en lista på ärenden-->
    <asp:GridView ID="CaseList" runat="server" GridLines="None" AutoGenerateColumns="false"  >
        <Columns>
            <asp:BoundField DataField="DateOfObservation" HeaderText="Ärende Anmält" />
            <asp:HyperLinkField DataTextField="ID" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="CrimeInvestigator.aspx?id={0}" HeaderText="Ärendenummer" />
            <asp:BoundField DataField="TypeOfCrime" HeaderText="Miljöbrott" />
            <asp:BoundField DataField="StatusName" HeaderText="Status" />
            <asp:BoundField DataField="DepartmentName" HeaderText="Avdelning" />
            <asp:BoundField DataField="EmployeeName" HeaderText="Handläggare" />
        </Columns>
    </asp:GridView>
</asp:Content>

