﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.IO;
using System.Globalization;

public partial class investigator_CrimeInvestigator : System.Web.UI.Page
{
    private SQLBackEnd _BackEnd = new SQLBackEnd();
    protected Case _CurrentCase;
    private string _StoreUrl;
    private Employee _User;

    protected void Page_Init()
    {
        _CurrentCase = _BackEnd.GetCase(Request.QueryString["id"]);
        _User = _BackEnd.GetEmployee(User.Identity.Name);

        //This is so the page don't crash due to nullpointer exception
        if (_CurrentCase == null)
        {
            Response.Redirect("~/Error.aspx?error=404");
        }
        // If the user does not have sufficient rights
        else if (_CurrentCase.EId != _User.ID)
        {
            Response.Redirect("~/Error.aspx?error=403");
        }

        // The external url that the samples and pictures is stored at. 
        _StoreUrl = WebConfigurationManager.ConnectionStrings["ftpPath"].ConnectionString + _CurrentCase.ID + "/";

        // This is so that the caseView has data to show
        HttpContext.Current.Items["Case"] = _CurrentCase;
        HttpContext.Current.Items["Samples"] = _BackEnd.GetSampleNames(_CurrentCase.ID);
        HttpContext.Current.Items["Pictures"] = _BackEnd.GetPictureNames(_CurrentCase.ID);
        HttpContext.Current.Items["StoreUrl"] = _StoreUrl;
    }

    protected void Page_Load(object sender, EventArgs e)
    { 
        //Loads items when the page is requested. On postbacks the page is 
        //populated by (ViewState?)
        if (!IsPostBack)
        {
            ActionTextBox.Text = _CurrentCase.Action;

            InfoTextBox.Text = _CurrentCase.Info;

            LongitudeHiddenField.Value = _CurrentCase.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            LatitudeHiddenField.Value = _CurrentCase.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);

            // Dropdown list
            PopulateDropDownList(_BackEnd.GetStatusList().ToList());
      }
    }

    /// <summary>
    /// Saves any updated information to the database and uploads any 
    /// provided samples and/ or pictures to the FTP-server. 
    /// </summary>
    /// <param name="sender">not used</param>
    /// <param name="e">not used</param>
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        LoadingImage.Visible = true;
        _CurrentCase.Action = ActionTextBox.Text;
        _CurrentCase.Info = InfoTextBox.Text;
        _CurrentCase.SId = StatusComboBox.SelectedValue;

        if(SampleFileUpload.HasFile || PictureFileUpload.HasFile)
        {
            var dir = Directory.CreateDirectory(Server.MapPath("~/temp/") + _CurrentCase.ID);
            var fm = new FtpManager();
            if (SampleFileUpload.HasFile)
            {
                SampleFileUpload.PostedFile.SaveAs(dir.FullName + "/" + SampleFileUpload.FileName);
                _BackEnd.InsertSample(_CurrentCase.ID, SampleFileUpload.FileName);
            }
            if (PictureFileUpload.HasFile)
            {
                PictureFileUpload.PostedFile.SaveAs(dir.FullName + "/" + PictureFileUpload.FileName);
                _BackEnd.InsertPicture(_CurrentCase.ID, PictureFileUpload.FileName);
            }
            fm.UploadAllFiles(dir);

            // I'm sorry Dave, I'm afraid I can't do that.
            // Directory.Delete(dir.FullName, true);
        }

        _BackEnd.UpdateCase(_CurrentCase);

        Response.Redirect(Request.RawUrl);
    }

    /// <summary>
    /// Add data to the dropdown lists.
    /// </summary>
    /// <param name="data">the data to add to the list</param>
    private void PopulateDropDownList(List<KeyValuePair<string, string>> data)
    {
        StatusComboBox.DataSource = data;
        StatusComboBox.DataTextField = "Value";
        StatusComboBox.DataValueField = "Key";
        StatusComboBox.DataBind();
    }
}