﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class citizen_validate : System.Web.UI.Page
{
    private static string FORM_COOKIE_NAME = "FormData";

    private SQLBackEnd _BackEnd = new SQLBackEnd();
    private HttpCookie _Cookie;

    protected void Page_Load(object sender, EventArgs e)
    {
        _Cookie = Request.Cookies[FORM_COOKIE_NAME];

        RestoreForm();
    }

    protected void ConfirmButton_Click(object sender, EventArgs e)
    {
        Case crimeCase = CookieToCase(_Cookie);
        string crimeId = "";

        crimeId = _BackEnd.AddCase(crimeCase);

        RemoveCookie(_Cookie);

        Response.Redirect("Thanks.aspx?crimeId=" + crimeId);
    }

    /// <summary>
    /// Fills the page with data from a cookie.
    /// </summary>
    private void RestoreForm()
    {
        if (_Cookie != null)
        {
            PlaceInfo.Text = HttpUtility.UrlDecode(_Cookie["Place"]);
            TypeOfCrimeInfo.Text = HttpUtility.UrlDecode(_Cookie["Crime"]);
            DateOfCrimeInfo.Text = HttpUtility.UrlDecode(_Cookie["CrimeDate"]);
            InformerNameInfo.Text = HttpUtility.UrlDecode(_Cookie["InformersName"]);
            InformerPhoneInfo.Text = HttpUtility.UrlDecode(_Cookie["InformersPhone"]);
            ObservationInfo.Text = HttpUtility.UrlDecode(_Cookie["Observation"]);
        }
        else
        {
            System.Diagnostics.Debug.WriteLine("Ingen kaka kunde laddas. ");
            Response.Redirect("~/Error.aspx?error=500");
        }
    }

    /// <summary>
    /// Turns a cookie to a case
    /// </summary>
    /// <param name="cookie">the input cookie</param>
    /// <returns>A case representing the cookie</returns>
    private Case CookieToCase(HttpCookie cookie)
    {
        Case tmpCase = new Case();

        tmpCase.Place = HttpUtility.UrlDecode(_Cookie["Place"]);
        tmpCase.TypeOfCrime = HttpUtility.UrlDecode(_Cookie["Crime"]);
        tmpCase.DateTimeOfObservation = DateTime.Parse(HttpUtility.UrlDecode(_Cookie["CrimeDate"]));
        tmpCase.InformerName = HttpUtility.UrlDecode(_Cookie["InformersName"]);
        tmpCase.InformerPhone = HttpUtility.UrlDecode(_Cookie["InformersPhone"]);
        tmpCase.Observation = HttpUtility.UrlDecode(_Cookie["Observation"]);

        return tmpCase;
    }

    /// <summary>
    /// Removes a cookie with the given name.
    /// </summary>
    /// <param name="name">the name of the cookie</param>
    private void RemoveCookie(HttpCookie cookie)
    {
        cookie.Expires = DateTime.Now.AddDays(-1);
        Response.Cookies.Add(cookie);
    }
}