﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Citizen.master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="citizen_about_services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <section id="mainColumn">
        <h2>Avdelningar och Tjänster</h2>
        <p class="info">Vi bryr oss om dig</p>

        <p>Folkhälsa - Hälso- och Sjukvård, Tandvård - Socialtjänst och omsorg - Trygghet och säkerhet</p>
        <p>Bygga och bo - Gator och parker - Kollektivtrafiken - Miljö- och hälsoskydd - Sophantering och återvinning</p>
        <p>Barn och utbildning - Kultur och fritid</p>
        <p>Företagsservice</p>
    </section>
    <!-- End Left -->
</asp:Content>

