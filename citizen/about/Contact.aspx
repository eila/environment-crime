﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Citizen.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="citizen_about_contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <section id="mainColumn">
        <h2>Kontaktuppgifter</h2>
        <p><span class="label">Besöksadress:</span> Storgatan 42</p>
        <p><span class="label">Postadress:</span> Box 10, 909 00 Småstad</p>
        <p><span class="label">Mail:</span> miljoboven@smastad.nu</p>
        <p><span class="label">Telefon:</span> 432-500 10 00</p>
    </section>
    <!-- End Left -->
</asp:Content>

