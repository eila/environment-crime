﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Citizen.master" AutoEventWireup="true" CodeFile="Thanks.aspx.cs" Inherits="citizen_thanks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <section id="mainColumn">
        <h2>Tack för din Anmälan</h2>
        <p class="info">I Småstad är inga brott för små för att anmälas</p>

        <p>
            Anmälan har nu skickats in till kommunen och kommer att utredas.
            <br />
            Vill du komplettera din anmälan kontakta oss via mail eller telefon.
            <br />
            Ange då nummer: <asp:Label ID="CrimeIdLabel" runat="server" Text=""></asp:Label>
        </p>
        <p><a class="button" href="~/Default.aspx" runat="server">Avsluta</a></p>
    </section>
    <!-- End Left -->
</asp:Content>

