﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    SQLBackEnd _BackEnd = new SQLBackEnd();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void LoginButton_Click(object sendeVr, EventArgs e)
    {
        var eId = _BackEnd.AuthenticateUser(UserNameTextBox.Text, PasswordTextBox.Text);
        
        if (eId == null)
        {
            VerificationLabel.Visible = true;
        }
        else
        {
            FormsAuthentication.RedirectFromLoginPage(eId, false);
            Response.Redirect(String.Format("~/{0}/start{0}.aspx", _BackEnd.GetEmployee(eId).Role));
        }
    }
}