﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Citizen.master" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" Runat="Server">
    <section id="mainColumn">
        <h2>Utloggning för Småstads personal</h2>
        <p class="info">Du är nu utloggad!</p>
    </section>
    <!-- End Left -->
</asp:Content>

