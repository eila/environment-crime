﻿<%@ Page Title="" Language="C#" MasterPageFile="~/coordinator/Coordinator.master" AutoEventWireup="true" CodeFile="Thanks.aspx.cs" Inherits="coordinator_Thanks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">

    <h2>Tack för din anmälan</h2>
    <p class="info">Du är inloggad som samordnare</p>

    <p>
        Anmälan har nu skickats in till kommunen och kommer att utredas.
        <br />
        Vill du komplettera din anmälan kontakta oss via mail eller telefon.
        <br />
        Ange då nummer: <asp:Label ID="CrimeIdLabel" runat="server" Text=""></asp:Label>
    </p>

    <p><a class="button" href="StartCoordinator.aspx" runat="server" >Avsluta rapporteringen</a></p>

</asp:Content>

