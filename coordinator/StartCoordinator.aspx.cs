﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class coordinator_StartCoordinator : System.Web.UI.Page
{
    private SQLBackEnd _BackEnd = new SQLBackEnd();
    string statusFilter, departmentFilter, idFilter;

    protected void Page_Load(object sender, EventArgs e)
    {
        statusFilter = Request.QueryString["sid"];
        departmentFilter = Request.QueryString["did"];
        idFilter = Request.QueryString["id"];

        if(!IsPostBack)
        { 
            //Status Dropdown list
            var statusList = _BackEnd.GetStatusList().ToList();
            statusList.Insert(0, _BackEnd.AllObject);
            PopulateDropDownList(StatusDropDown, statusList);

            //Department Dropdown list
            var DepartmentList = _BackEnd.GetDepartmentList().ToList();
            DepartmentList.Insert(0, _BackEnd.AllObject);
            PopulateDropDownList(DepartmentDropDown, DepartmentList);

            StatusDropDown.SelectedValue = statusFilter;
            DepartmentDropDown.SelectedValue = departmentFilter;
            CaseIdSearchBox.Text = idFilter;
        }

        PopulateCaseList();
    }

    protected void OtherFilterButton_Click(object sender, EventArgs e)
    {
        statusFilter = StatusDropDown.SelectedValue;
        departmentFilter = DepartmentDropDown.SelectedValue;
        UpdateList();
    }

    protected void IdFilterButton_Click(object sender, EventArgs e)
    {
        idFilter = CaseIdSearchBox.Text;
        UpdateList();
    }

    protected void CaseList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaseList.PageIndex = e.NewPageIndex;
        CaseList.DataBind();
    }

    /// <summary>
    /// Populates a given dropdownlist with given values. the provided values 
    /// must be a list with KeyValuePairs. Key the dropdowns data field and 
    /// value the text of each element.
    /// </summary>
    /// <param name="list">dropdownlist to populate</param>
    /// <param name="data">to fill the dropdownlist with</param>
    private void PopulateDropDownList(DropDownList list, List<KeyValuePair<string, string>> data)
    {
        list.DataSource = data;
        list.DataTextField = "Value";
        list.DataValueField = "Key";
        list.DataBind();
    }

    // The cases in main screen
    private void PopulateCaseList()
    {
        CaseList.DataSource = _BackEnd.GetCaseList()
            .Where(c => statusFilter == null || c.SId == statusFilter || statusFilter == _BackEnd.AllObject.Key)
            .Where(c => departmentFilter == null || c.DId == departmentFilter || departmentFilter == _BackEnd.AllObject.Key)
            .Where(c => idFilter == null || c.ID.Contains(idFilter))
            .ToList();
        CaseList.DataBind();
    }


    private void UpdateList()
    {
        Response.Redirect("StartCoordinator.aspx?sid=" + statusFilter + "&did=" + departmentFilter + "&id=" + idFilter);
    }
}