﻿<%@ Page Title="" Language="C#" MasterPageFile="~/coordinator/Coordinator.master" AutoEventWireup="true" CodeFile="CrimeCoordinator.aspx.cs" Inherits="coordinator_CrimeCoordinator" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Web_Controls/CaseDetails.ascx" TagPrefix="uc1" TagName="CaseDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <h2>Detaljer för ärende: <asp:Label ID="CaseIdLabel" runat="server" Text=""></asp:Label></h2>
    <p class="info">Du är inloggad som samordnare</p>

        <div id="map">
            <p class="error">Det ska vara en karta här. Är javascript aktiverat?</p>
        </div>

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDfy0hZbkGoDGgmJitd8CGd5nNG60OZzBE"></script>
        <script type="text/javascript" src="../js/Map.js"></script>

        <input type="hidden" id="LongitudeHiddenField" value="" runat="server" />
        <input type="hidden" id="LatitudeHiddenField" value="" runat="server" />

        <p class="info">Du kan dra i markören för att ändra plats.</p>

    <uc1:CaseDetails runat="server" ID="CaseDetails"></uc1:CaseDetails>

    <section id="bottomColumn">
        <h3>Redigera ärende</h3>
        <input type="hidden" id="CanEditMapMarker" value="true" />
        <p>
            Ange enhet: 
            <ajaxToolkit:ComboBox ID="DepartmentComboBox" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"></ajaxToolkit:ComboBox>
        </p>
        <p>
            <asp:Button ID="SaveButton" CssClass="button" runat="server" OnClick="SaveButton_Click" Text="Spara ändringarna" />
        </p>
    </section>
</asp:Content>

