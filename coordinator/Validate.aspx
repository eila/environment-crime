﻿<%@ Page Title="" Language="C#" MasterPageFile="~/coordinator/Coordinator.master" AutoEventWireup="true" CodeFile="Validate.aspx.cs" Inherits="coordinator_Validate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <h2>Din anmälan av miljöbrott</h2>
    <p class="info">Du är inloggad som samordnare</p>

    <p class="label">
        Vänligen läs igenom din anmälan och se att allt är okej.<br />
        Om något är fel, gå tillbaka och ändra. Annars välj Bekräfta.
    </p>
    <p>
        <span class="label">Var har brottet skett någonstans?</span><br />
        <asp:Label ID="PlaceInfo" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <span class="label">Vilken typ av brott?</span><br />
        <asp:Label ID="TypeOfCrimeInfo" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <span class="label">När skedde brottet?</span><br />
        <asp:Label ID="DateOfCrimeInfo" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <span class="label">Ditt namn (för- och efternamn):</span><br />
        <asp:Label ID="InformerNameInfo" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <span class="label">Din telefon:</span><br />
        <asp:Label ID="InformerPhoneInfo" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <span class="label">Mer information:</span><br />
        <asp:Label ID="ObservationInfo" runat="server" Text="Label"></asp:Label>
    </p>
    <p>
        <a class="button" href="ReportCrime.aspx" runat="server" id="link">Tillbaka</a>
        <asp:Button ID="ConfirmButton" CssClass="button" runat="server" Text="Bekräfta" OnClick="ConfirmButton_Click" />
    </p>
</asp:Content>

