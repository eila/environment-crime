﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class coordinator_CrimeCoordinator : System.Web.UI.Page
{
    private SQLBackEnd _BackEnd = new SQLBackEnd();
    protected Case _CurrentCase;
    private string _StoreUrl;

    protected void Page_Init()
    {
        _CurrentCase = _BackEnd.GetCase(Request.QueryString["id"]);

        //This is so the page don't crash due to nullpointer exception
        if (_CurrentCase == null)
        {
            Response.Redirect("~/Error.aspx?error404");
        }

        // The external url that the samples and pictures is stored at. 
        _StoreUrl = WebConfigurationManager.ConnectionStrings["ftpPath"].ConnectionString + _CurrentCase.ID + "/";

        // This is so that the caseView has data to show
        HttpContext.Current.Items["Case"] = _CurrentCase;
        HttpContext.Current.Items["Samples"] = _BackEnd.GetSampleNames(_CurrentCase.ID);
        HttpContext.Current.Items["Pictures"] = _BackEnd.GetPictureNames(_CurrentCase.ID);
        HttpContext.Current.Items["StoreUrl"] = _StoreUrl;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            CaseIdLabel.Text = _CurrentCase.ID;
            LongitudeHiddenField.Value = _CurrentCase.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            LatitudeHiddenField.Value = _CurrentCase.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);

            var departmentList = _BackEnd.GetDepartmentList().ToList();
            departmentList.Insert(0, _BackEnd.NoneObject);

            PopulateDropDownList(departmentList);
            // Sets the correct department
            DepartmentComboBox.SelectedValue = _CurrentCase.DId;
        }
    }

    /// <summary>
    /// Saves any updated information to the database.
    /// </summary>
    /// <param name="sender">not used</param>
    /// <param name="e">not used</param>
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        _CurrentCase.DId = DepartmentComboBox.SelectedValue;
        _CurrentCase.Longitude = double.Parse(LongitudeHiddenField.Value, CultureInfo.InvariantCulture);
        _CurrentCase.Latitude = double.Parse(LatitudeHiddenField.Value, CultureInfo.InvariantCulture);

        _BackEnd.UpdateCase(_CurrentCase);

        Response.Redirect(Request.RawUrl);
    }    

    /// <summary>
    /// Add data to the dropdown lists.
    /// </summary>
    /// <param name="data">the data to add to the list</param>
    private void PopulateDropDownList(List<KeyValuePair<string, string>> data)
    {
        DepartmentComboBox.DataSource = data;
        DepartmentComboBox.DataTextField = "Value";
        DepartmentComboBox.DataValueField = "Key";
        DepartmentComboBox.DataBind();
    }
}