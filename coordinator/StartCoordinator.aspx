﻿<%@ Page Title="" Language="C#" MasterPageFile="~/coordinator/Coordinator.master" AutoEventWireup="true" CodeFile="StartCoordinator.aspx.cs" Inherits="coordinator_StartCoordinator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <h2>Ärenden</h2>
    <p class="info">Du är inloggad som samordnare</p>

    <table id="managerForm">
        <tr>
            <td class="label">Välj status:</td>
            <td class="label">Välj avdelning:</td>
            <td>&nbsp;</td>
            <td class="label">Ärendenummer:</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="StatusDropDown" runat="server"></asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="DepartmentDropDown" runat="server"></asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="OtherFilterButton" CssClass="button" Text="Hämta lista" OnClick="OtherFilterButton_Click" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="CaseIdSearchBox" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="IdFilterButton" CssClass="button" Text="Hämta lista" OnClick="IdFilterButton_Click" runat="server" />
            </td>
        </tr>
    </table>

    <!--Nedan ser man en lista på ärenden-->
    <asp:GridView ID="CaseList" 
        GridLines="None" AutoGenerateColumns="false" EmptyDataText="Inga fall att rapportera!" 
        AllowPaging="true" OnPageIndexChanging="CaseList_PageIndexChanging" PageSize="100" runat="server" >
        <Columns>
            <asp:BoundField DataField="DateOfObservation" HeaderText="Ärende Anmält" />
            <asp:HyperLinkField DataTextField="ID" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="CrimeCoordinator.aspx?id={0}" HeaderText="Ärendenummer" />
            <asp:BoundField DataField="TypeOfCrime" HeaderText="Miljöbrott" />
            <asp:BoundField DataField="StatusName" HeaderText="Status" />
            <asp:BoundField DataField="DepartmentName" HeaderText="Avdelning" />
            <asp:BoundField DataField="EmployeeName" HeaderText="Handläggare" />
        </Columns>
    </asp:GridView>
</asp:Content>

