﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class coordinator_ReportCrime : System.Web.UI.Page
{
    private static string FORM_COOKIE_NAME = "FormData";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RestoreForm();
        }
        setDateCalendarRange();
    }

    /// <summary>
    /// Sets the Calendar to make sure its between three months ago and today.
    /// </summary>
    private void setDateCalendarRange()
    {
        //Makes sure that the calendar is between today or 3 months ago. 
        CrimeDateCalendarExtender.StartDate = DateTime.Today.AddMonths(-3);
        CrimeDateCalendarExtender.EndDate = DateTime.Today;
    }

    protected void SendButton_Click(object sender, EventArgs e)
    {
        SaveForm();
        Response.Redirect("Validate.aspx");
    }

    /// <summary>
    /// Fills the form on the page with data from a cookie.
    /// </summary>
    private void RestoreForm()
    {
        HttpCookie cookie = Request.Cookies[FORM_COOKIE_NAME];

        if (cookie != null)
        {
            PlaceTextBox.Text = HttpUtility.UrlDecode(cookie["Place"]);
            CrimeTextBox.Text = HttpUtility.UrlDecode(cookie["Crime"]);
            CrimeDateTextBox.Text = HttpUtility.UrlDecode(cookie["CrimeDate"]);
            InformersNameTextBox.Text = HttpUtility.UrlDecode(cookie["InformersName"]);
            InformersPhoneTextBox.Text = HttpUtility.UrlDecode(cookie["InformersPhone"]);
            ObservationTextBox.Text = HttpUtility.UrlDecode(cookie["Observation"]);
        }
    }

    /// <summary>
    /// Saves the data in the form as a cookie.
    /// </summary>
    private void SaveForm()
    {
        Response.Cookies.Remove(FORM_COOKIE_NAME);
        HttpCookie cookie = new HttpCookie(FORM_COOKIE_NAME);

        //Sets the expire date for the cookie. None for session only.
        //cookie.Expires = DateTime.Now.AddDays(7);

        cookie["Place"] = HttpUtility.UrlEncode(PlaceTextBox.Text);
        cookie["Crime"] = HttpUtility.UrlEncode(CrimeTextBox.Text);
        cookie["CrimeDate"] = HttpUtility.UrlEncode(CrimeDateTextBox.Text);
        cookie["InformersName"] = HttpUtility.UrlEncode(InformersNameTextBox.Text);
        cookie["InformersPhone"] = HttpUtility.UrlEncode(InformersPhoneTextBox.Text);
        cookie["Observation"] = HttpUtility.UrlEncode(ObservationTextBox.Text);

        Response.Cookies.Add(cookie);
    }
}