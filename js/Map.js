﻿/*******************
    Creates a map
*******************/
(function()
{
    "use strict";
    var mapCanvas = document.getElementById("map");
    mapCanvas.replaceChild(
        document.createTextNode("Laddar karta..."),
        mapCanvas.firstElementChild);

    window.onload = function () {

        var latField = document.getElementById("Content_LatitudeHiddenField");
        var lngField = document.getElementById("Content_LongitudeHiddenField");

        var pos = {
            lat: Number(latField.value.replace(",", ".")),
            lng: Number(lngField.value.replace(",", "."))
        };

        var options = {
            zoom: 16,
            center: pos
        };

        var map = new google.maps.Map(mapCanvas, options);
        
        var marker = new google.maps.Marker({ 
            position: pos,
            map: map 
        });

        if (document.getElementById("CanEditMapMarker"))
        {
            marker.setDraggable(true);
        }

        // Update the lat/ long-fields so that other code can access it.
        marker.addListener('dragend', function () {
            latField.value = marker.position.lat();
            lngField.value = marker.position.lng();
        });
    }
}())
