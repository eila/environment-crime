﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Citizen.master" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" Runat="Server">
    <section id="mainColumn">
        <h2 id="ErrorTitle" runat="server">Okänt fel</h2>
        <p id="ErrorMessage" runat="server"></p>
    </section>
</asp:Content>

