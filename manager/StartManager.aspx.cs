﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manager_StartManager : System.Web.UI.Page
{
    SQLBackEnd _BackEnd = new SQLBackEnd();
    string statusFilter, employeeFilter, idFilter;
    Employee _User;

    protected void Page_Load(object sender, EventArgs e)
    {
        statusFilter = Request.QueryString["sid"];
        employeeFilter = Request.QueryString["eid"];
        idFilter = Request.QueryString["id"];

        _User = _BackEnd.GetEmployee(User.Identity.Name);

        if (!IsPostBack)
        {
            //Status Dropdown list
            var statusList = _BackEnd.GetStatusList().ToList();
            statusList.Insert(0, _BackEnd.AllObject);
            ControlPopulator.PopulateDropDownList(StatusDropDown, statusList);

            //Employee Dropdown list
            var investigatorList = _BackEnd.GetInvestigatorList()
                .Where(em => em.Role == "investigator")
                .Where(em => _User.DId == em.DId)
                .ToList()
                .ConvertAll(em => new KeyValuePair<string, string>(em.ID, em.FullName));
            investigatorList.Insert(0, _BackEnd.AllObject);
            ControlPopulator.PopulateDropDownList(EmployeeDropDown, investigatorList);

            StatusDropDown.SelectedValue = statusFilter;
            EmployeeDropDown.SelectedValue = employeeFilter;
            CaseIdSearchBox.Text = idFilter;
        }

        PopulateCaseList();
    }

    protected void OtherFilterButton_Click(object sender, EventArgs e)
    {
        statusFilter = StatusDropDown.SelectedValue;
        employeeFilter = EmployeeDropDown.SelectedValue;
        UpdateList();
    }

    protected void IdFilterButton_Click(object sender, EventArgs e)
    {
        idFilter = CaseIdSearchBox.Text;
        UpdateList();
    }

    protected void CaseList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        CaseList.PageIndex = e.NewPageIndex;
        CaseList.DataBind();
    }

    /// <summary>
    /// Populates the cases on the main screen.
    /// </summary>
    private void PopulateCaseList()
    {
        CaseList.DataSource = _BackEnd.GetCaseList()
            // The manager can only sees his departments cases
            .Where(c => c.DId == _User.DId)
            // The users filter
            .Where(c => statusFilter == null || c.SId == statusFilter || statusFilter == _BackEnd.AllObject.Key)
            .Where(c => employeeFilter == null || c.EId == employeeFilter || employeeFilter == _BackEnd.AllObject.Key)
            .Where(c => idFilter == null || c.ID.Contains(idFilter))
            .ToList();
        CaseList.DataBind();
    }

    /// <summary>
    /// Refreshes the page with the selected filters.
    /// </summary>
    private void UpdateList()
    {
        Response.Redirect("StartManager.aspx?sid=" + statusFilter + "&eid=" + employeeFilter + "&id=" + idFilter);
    }

}