﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manager_CrimeManager : System.Web.UI.Page
{
    private SQLBackEnd _BackEnd = new SQLBackEnd();
    protected Case _CurrentCase;
    private string _StoreUrl;
    private Employee _User;

    protected void Page_Init()
    { 
        _CurrentCase = _BackEnd.GetCase(Request.QueryString["id"]);
        _User = _BackEnd.GetEmployee(User.Identity.Name);

        // This is so the page don't crash due to nullpointer exception
        if (_CurrentCase == null)
        {
            Response.Redirect("~/Error.aspx?error=404");
        }
        // If the user does not have sufficient rights
        else if(_CurrentCase.DId != _User.DId)
        {
            Response.Redirect("~/Error.aspx?error=403");
        }

        // The external url that the samples and pictures is stored at. 
        _StoreUrl = WebConfigurationManager.ConnectionStrings["ftpPath"].ConnectionString + _CurrentCase.ID + "/";

        // This is so that the caseView has data to show
        HttpContext.Current.Items["Case"] = _CurrentCase;
        HttpContext.Current.Items["Samples"] = _BackEnd.GetSampleNames(_CurrentCase.ID);
        HttpContext.Current.Items["Pictures"] = _BackEnd.GetPictureNames(_CurrentCase.ID);
        HttpContext.Current.Items["StoreUrl"] = _StoreUrl;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            PopulateDropDownLists();
            PopulateInput();
            LongitudeHiddenField.Value = _CurrentCase.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
            LatitudeHiddenField.Value = _CurrentCase.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat);
        }
    }
        
    /// <summary>
    /// Add data to the dropdown lists.
    /// </summary>
    private void PopulateDropDownLists()
    {
        // Investigator list
        var investigatorList = _BackEnd.GetInvestigatorList()
            .Where(em => em.Role == "investigator")
            .Where(em => _User.DId == em.DId)
            .ToList()
            .ConvertAll(em => new KeyValuePair<string, string>(em.ID, em.FullName));
        investigatorList.Insert(0, _BackEnd.NoneObject);
        PopulateDropDownList(investigatorList);
    }

    /// <summary>
    /// Inserts data from the case into the edit fields.
    /// </summary>
    private void PopulateInput()
    {
        EmployeeComboBox.SelectedValue = _CurrentCase.EId;
        InfoCheckBox.Checked = _CurrentCase.SId == "S_B" ? true : false;
        InfoTextBox.Text = _CurrentCase.Info;
    }


    /// <summary>
    /// Saves the data that have been written in and reloads the page with new
    /// information.
    /// </summary>
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        _CurrentCase.EId = EmployeeComboBox.SelectedValue;
        if (InfoCheckBox.Checked)
        {
            _CurrentCase.SId = "S_B";
            _CurrentCase.Info = InfoTextBox.Text;
        }

        _BackEnd.UpdateCase(_CurrentCase);

        Response.Redirect(Request.RawUrl);
    } 


    /// <summary>
    /// Add data to the dropdown lists.
    /// </summary>
    /// <param name="data">the data to add</param>
    private void PopulateDropDownList(List<KeyValuePair<string, string>> data)
    {
        EmployeeComboBox.DataSource = data;
        EmployeeComboBox.DataTextField = "Value";
        EmployeeComboBox.DataValueField = "Key";
        EmployeeComboBox.DataBind();
    }

}