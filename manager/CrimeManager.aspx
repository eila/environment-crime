﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/Manager.master" AutoEventWireup="true" CodeFile="CrimeManager.aspx.cs" Inherits="manager_CrimeManager" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Web_Controls/CaseDetails.ascx" TagPrefix="uc1" TagName="CaseDetails" %>

<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="Server">
    <h2>Detaljer för ärende: <%# _CurrentCase.ID %></h2>
    <p class="info">Du är inloggad som avdelningschef</p>

    <div id="map">
        <p class="error">Det ska vara en karta här. Är javascript aktiverat?</p>
    </div>

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDfy0hZbkGoDGgmJitd8CGd5nNG60OZzBE"></script>
    <script type="text/javascript" src="../js/Map.js"></script>

    <input type="hidden" id="LongitudeHiddenField" value="" runat="server" />
    <input type="hidden" id="LatitudeHiddenField" value="" runat="server" />

    <uc1:CaseDetails runat="server" ID="CaseDetails"></uc1:CaseDetails>

    <section id="bottomColumn">
        <h3>Redigera ärende</h3>
        <p>
            Ange handläggare: 
            <ajaxToolkit:ComboBox ID="EmployeeComboBox" runat="server" DropDownStyle="DropDownList" AutoCompleteMode="SuggestAppend"></ajaxToolkit:ComboBox>
        </p>
        <p>
            <asp:CheckBox ID="InfoCheckBox" runat="server" />
            Ingen åtgärd:
            <asp:TextBox ID="InfoTextBox" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Button ID="SaveButton" CssClass="button" runat="server" OnClick="SaveButton_Click" Text="Spara" />
        </p>
    </section>

</asp:Content>