﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Error : System.Web.UI.Page
{
    private string _ErrorCode;

    protected void Page_Load(object sender, EventArgs e)
    {
        _ErrorCode = Request.QueryString["error"];
        ErrorTitle.InnerText = "Fel: " + _ErrorCode;

        switch (_ErrorCode)
        {
            case "400":
                ErrorMessage.InnerText = "Url:en är konstig. ";
                break;
            case "403":
                ErrorMessage.InnerText = "Otillräckliga rättigheter";
                break;
            case "404":
                ErrorMessage.InnerText = "Denna sida finns inte. ";
                break;
            case "500":
                ErrorMessage.InnerText = "Servern har blivit sjuk. Kontakta datorläkaren (systemadministratören). ";
                break;
            default:
                ErrorMessage.InnerText = "Okänt fel. ";
                break;
        }
    }
}