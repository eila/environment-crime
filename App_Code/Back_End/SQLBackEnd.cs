﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Web.Configuration;
using System.Text.RegularExpressions;

/// <summary>
/// Handles connections to a MySQL server
/// </summary>
public class SQLBackEnd
{
    private MySqlConnection _Connection;

    /// <summary>
    /// Constructs a new SQLBackEnd.
    /// </summary>
    public SQLBackEnd()
    {
        _Connection = new MySqlConnection(WebConfigurationManager.ConnectionStrings["environmentCrime"].ConnectionString);
    }

    /// <summary>
    /// Returns an object that refers to *everything* in the same style as the
    /// objects in the lists returned by the functions `getDepartmentList()`,
    /// `getEmployeeList` and `getStatusList()`.
    /// </summary>
    public KeyValuePair<string, string> AllObject
    {
        get
        {
            return new KeyValuePair<string, string>("All", "Välj Alla");
        }
    }

    /// <summary>
    /// Returns an object that refers to *nothing* in the same style as the
    /// objects in the lists returned by the functions `getDepartmentList()`,
    /// `getEmployeeList` and `getStatusList()`.
    /// </summary>
    public KeyValuePair<string, string> NoneObject
    {
        get
        {
            return new KeyValuePair<string, string>("", "Ingen Vald");
        }
    }

    /// <summary>
    /// Adds a given case to the database. Only values from the given list will be added:
    /// - Place
    /// - TypeOfCrime
    /// - DateTimeOfObservation
    /// - Observation
    /// - InformerName
    /// - InformerPhone
    /// </summary>
    /// <param name="the case to add"></param>
    /// <returns>the caseId for the added case</returns>
    public string AddCase(Case inCase)
    {
        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText = "insertCrime;";
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@newPlace", inCase.Place);
        cmd.Parameters.AddWithValue("@newTypeOfCrime", inCase.TypeOfCrime);
        cmd.Parameters.AddWithValue("@newDateOfObservation", inCase.DateTimeOfObservation);
        cmd.Parameters.AddWithValue("@newObservation", inCase.Observation);
        cmd.Parameters.AddWithValue("@newInformerName", inCase.InformerName);
        cmd.Parameters.AddWithValue("@newInformerPhone", inCase.InformerPhone);
        cmd.Parameters.AddWithValue("@newStatusId", "S_A");

        cmd.Parameters.Add(new MySqlParameter("@newCrimeId", MySqlDbType.VarChar, 12));
        cmd.Parameters["@newCrimeId"].Direction = System.Data.ParameterDirection.Output;

        _Connection.Open();

        cmd.ExecuteNonQuery();

        return (string)cmd.Parameters["@newCrimeId"].Value;

    }

    /// <summary>Returns one specific Case. </summary>
    /// <param name="productID">The id of the Case</param>
    /// <returns>The specific Case or null if none was found</returns>
    public Case GetCase(string caseID) 
    {
        Case tmpCase = null;

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT crime.*, status.Status, departments.DName, CONCAT(employees.FName,' ',employees.LName) AS EName \n" +
            "FROM crime \n" +
            "LEFT JOIN status \n" +
            "ON crime.SId=status.SId \n" +
            "LEFT JOIN departments \n" +
            "ON crime.DId=departments.DId \n" +
            "LEFT JOIN employees \n" +
            "ON crime.EId=employees.EId \n" +
            "WHERE CId='" + caseID + "' \n" +
            ";";

            _Connection.Open();

            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                tmpCase = new Case();

                tmpCase.ID = reader["CId"].ToString();
                tmpCase.Place = reader["Place"].ToString();
                tmpCase.Latitude = reader.GetDouble("Latitud");
                tmpCase.Longitude = reader.GetDouble("Longitud");
                tmpCase.DateTimeOfObservation = reader.GetDateTime("DateOfObservation");
                tmpCase.TypeOfCrime = reader["TypeOfCrime"].ToString();
                tmpCase.Observation = reader["Observation"].ToString();
                tmpCase.Info = reader["Info"].ToString();
                tmpCase.Action = reader["Action"].ToString();
                tmpCase.InformerName = reader["InformerName"].ToString();
                tmpCase.InformerPhone = reader["InformerPhone"].ToString();
                tmpCase.StatusName = reader["Status"].ToString();
                tmpCase.SId = reader["SId"].ToString();
                tmpCase.DepartmentName = reader["DName"].ToString();
                tmpCase.DId = reader["DId"].ToString();
                tmpCase.EmployeeName = reader["EName"].ToString();
                tmpCase.EId = reader["EId"].ToString();

            }

            reader.Close();
        _Connection.Close();

        return tmpCase;
    }

    /// <summary>Return the whole Case List, sorted by ID.</summary>
    /// <returns>The Case List, sorted by ID</returns>
    public IEnumerable<Case> GetCaseList()
    {
        var caseList = new List<Case>();

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT crime.*, status.Status, departments.DName, CONCAT(employees.FName,' ',employees.LName) AS EName \n" +
            "FROM crime \n" +
            "LEFT JOIN status \n" +
            "ON crime.SId=status.SId \n" +
            "LEFT JOIN departments \n" +
            "ON crime.DId=departments.DId \n" +
            "LEFT JOIN employees \n" +
            "ON crime.EId=employees.EId \n" +
            "ORDER BY CId DESC \n" +
            ";";

            _Connection.Open();

            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                var tmpCase = new Case();

                tmpCase.ID = reader["CId"].ToString();
                tmpCase.Place = reader["Place"].ToString();
                tmpCase.DateTimeOfObservation = reader.GetDateTime("DateOfObservation");
                tmpCase.TypeOfCrime = reader["TypeOfCrime"].ToString();
                tmpCase.Observation = reader["Observation"].ToString();
                tmpCase.Info = reader["Info"].ToString();
                tmpCase.Action = reader["Action"].ToString();
                tmpCase.InformerName = reader["InformerName"].ToString();
                tmpCase.InformerPhone = reader["InformerPhone"].ToString();
                tmpCase.StatusName = reader["Status"].ToString();
                tmpCase.SId = reader["SId"].ToString();
                tmpCase.DepartmentName = reader["DName"].ToString();
                tmpCase.DId = reader["DId"].ToString();
                tmpCase.EmployeeName = reader["EName"].ToString();
                tmpCase.EId = reader["EId"].ToString();

                caseList.Add(tmpCase);
            }

            reader.Close();
            _Connection.Close();

        return caseList;
    }

    /// <summary>Method to return the whole Department List</summary>
    /// <returns>The Department List, represented by an object with properties Name, and Id</returns>
    public IEnumerable<KeyValuePair<string, string>> GetDepartmentList()
    {
        var departmentList = new List<KeyValuePair<string, string>>();

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT departments.DName, departments.DId \n" +
            "FROM departments \n" +
            ";";

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            departmentList.Add(new KeyValuePair<string, string>(
                reader.GetString("DId"), reader.GetString("DName")));
        }

        reader.Close();
        _Connection.Close();

        return departmentList;
    }

    /// <summary>Method to return the whole Investigator List</summary>
    /// <returns>The Investigator List, represented by an object with properties EName, and EId</returns>
    public IEnumerable<Employee> GetInvestigatorList()
    {
        var employeeList = new List<Employee>();

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT * \n" +
            "FROM employees \n" +
            "WHERE employees.Role='investigator' \n" +
            ";";

            _Connection.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
            employeeList.Add(new Employee(
                reader.GetString("EId"), 
                reader.GetString("FName"),
                reader.GetString("LName"),
                reader.GetString("Role"),
                reader.GetString("DId")
                ));
            }

            reader.Close();
            _Connection.Close();

        return employeeList;
    }

    /// <summary>Method to return the whole Status List</summary>
    /// <returns>The Status List, represented by an object with properties Status, and SId</returns>
    public IEnumerable<KeyValuePair<string, string>> GetStatusList()
    {
        var statusList = new List<KeyValuePair<string, string>>();

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT status.Status, status.SId \n" +
            "FROM status \n" +
            ";";

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            statusList.Add(new KeyValuePair<string, string>(
                reader.GetString("SId"), reader.GetString("Status")));
        }

        reader.Close();
        _Connection.Close();

        return statusList;
    }

    /// <summary>
    /// Returns all picture names associated with a given case.
    /// </summary>
    /// <param name="caseID"></param>
    /// <returns>paths</returns>
    public IEnumerable<string> GetPictureNames(string caseID)
    {
        var pictureList = new List<string>();

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT * \n" +
            "FROM pictures \n" +
            "WHERE CId = '" + caseID + "' \n" +
            ";";

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            pictureList.Add(reader.GetString("PName"));
        }

        reader.Close();
        _Connection.Close();

        return pictureList;
    }

    /// <summary>
    /// Returns all sample names associated with a given case.
    /// </summary>
    /// <param name="caseID"></param>
    /// <returns>Filenames</returns>
    public IEnumerable<string> GetSampleNames(string caseID)
    {
        var sampleList = new List<string>();

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT * \n" +
            "FROM samples \n" +
            "WHERE CId = '" + caseID + "' \n" +
            ";";

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            sampleList.Add(reader.GetString("SpName"));
        }

        reader.Close();
        _Connection.Close();

        return sampleList;
    }

    /// <summary>
    /// Returns a object representing a specific employee.
    /// </summary>
    /// <param name="id">the id of the employee to get</param>
    /// <returns>object representing the given employee</returns>
    public Employee GetEmployee(string id)
    {
        Employee employee = null;

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT * \n" +
            "FROM employees \n" +
            "WHERE EId =@eId \n" +
            ";";

        cmd.Parameters.AddWithValue("@eId", id);

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            employee = new Employee(
                reader.GetString("EId"),
                reader.GetString("FName"),
                reader.GetString("LName"),
                reader.GetString("Role"),
                reader.GetString("DId"));
        }

        reader.Close();
        _Connection.Close();

        return employee;
    }

    /// <summary>
    /// Checks if a given credentials are valid, if so returns the associated
    /// id of the credentials, else returns null.
    /// </summary>
    /// <param name="userName">the username</param>
    /// <param name="password">the password</param>
    /// <returns>the employees id associated with the credentials, or null if the login was invalid</returns>
    public string AuthenticateUser(string userName, string password)
    {
        string id = null;

        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;
        cmd.CommandText =
            "SELECT logins.* \n" +
            "FROM logins \n" +
            "WHERE UserName=@userName \n" +
            "AND Password=@password \n" +
            ";";

        cmd.Parameters.AddWithValue("@userName", userName);
        cmd.Parameters.AddWithValue("@password", password);

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            id = reader["EId"].ToString();
        }

        reader.Close();
        _Connection.Close();

        return id;
    }

    /// <summary>
    /// Replaces/ updates a case with new values. The case will overwrite
    /// another case with the same ID. The only values that will be changed 
    /// are:
    ///  - Info
    ///  - Action
    ///  - Latitud
    ///  - Longitud
    ///  - SId
    ///  - DId
    ///  - EId
    /// Note that Status, Employee and Department values will not be used, only
    /// IDs will.
    /// </summary>
    /// <param name="inCase">the case to update</param>
    public void UpdateCase(Case inCase)
    {
        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;

            cmd.CommandText =
            "UPDATE crime \n" +
            "SET Info = @info, \n" + 
                "Action = @action, \n" + 
                "Latitud = @latitude, \n" + 
                "Longitud = @longitude, \n" + 
                "SId = @sId, \n" + 
                "DId = @dId, \n" + 
                "EId = @eId \n" + 
            "WHERE CId=@id " +
            ";";

            cmd.Parameters.AddWithValue("@id", inCase.ID);
            cmd.Parameters.AddWithValue("@info", inCase.Info);
            cmd.Parameters.AddWithValue("@action", inCase.Action);
            cmd.Parameters.AddWithValue("@latitude", inCase.Latitude);
            cmd.Parameters.AddWithValue("@longitude", inCase.Longitude);
            cmd.Parameters.AddWithValue("@sId", inCase.SId);
            cmd.Parameters.AddWithValue("@dId", inCase.DId);
            cmd.Parameters.AddWithValue("@eId", inCase.EId);

            _Connection.Open();

            var reader = cmd.ExecuteReader();

            reader.Close();
            _Connection.Close();
    }

    /// <summary>
    /// Inserts a sample into the database, associated with a given case.
    /// </summary>
    /// <param name="caseId">caseId of the associated file</param>
    /// <param name="sampleName">filename of the sample</param>
    public void InsertSample(string caseId, string sampleName)
    {
        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;

        cmd.CommandText =
        "INSERT INTO samples (CId, SPName) \n" +
        "VALUES ('" + caseId + "', '" + sampleName + "') \n" +
        ";";

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        reader.Close();
        _Connection.Close();
    }

    /// <summary>
    /// Inserts a picture into the database, associated with a given case.
    /// </summary>
    /// <param name="caseId">caseId of the associated file</param>
    /// <param name="pictureName">filename of the picture</param>
    public void InsertPicture(string caseId, string pictureName)
    {
        var cmd = new MySqlCommand();
        cmd.Connection = _Connection;

        cmd.CommandText =
        "INSERT INTO pictures (CId, PName)\n" +
        "VALUES ('" + caseId + "', '" + pictureName + "')\n" +
        ";";

        _Connection.Open();

        var reader = cmd.ExecuteReader();

        reader.Close();
        _Connection.Close();
    }
}