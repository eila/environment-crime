﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// A class representing an employee.
/// </summary>
public class Employee
{
    private string _Id,
        _FirstName,
        _LastName,
        _Role,
        _DId;

    public Employee(string id, string firstName, string lastName, string role, string dId)
    {
        _Id = id;
        _FirstName = firstName;
        _LastName = lastName;
        _Role = role;
        _DId = dId;
    }

    public string ID
    {
        get { return _Id; }
    }

    public string FullName
    {
        get { return String.Format("{0} {1}", _FirstName, _LastName); }
    }

    public string Role
    {
        get { return _Role; }
    }

    public string DId
    {
        get { return _DId; }
    }
}