﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControlPopulator
/// </summary>
public static class ControlPopulator
{
    public static void PopulateDropDownList(DropDownList list, List<KeyValuePair<string, string>> data)
    {
        list.DataSource = data;
        list.DataTextField = "Value";
        list.DataValueField = "Key";
        list.DataBind();
    }
}