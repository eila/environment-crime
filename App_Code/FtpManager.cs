﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// Uploads files to a ftpserver.
/// </summary>
public class FtpManager
{
    private static string _FtpUrl = "ftp://130.238.174.237:2121/";
    private WebClient _FtpServer = new WebClient();
    
    public FtpManager()
    {
        _FtpServer.Credentials = new NetworkCredential("ftpuser", "KHR3ENJjMh");
    }

    /// <summary>
    /// Creates a folder on the FTPserver if needed and adds all files in a folder to it.
    /// </summary>
    /// <param name="dir">the directory on the server to add files to</param>
    public void UploadAllFiles(DirectoryInfo dir)
    {
        var request = FtpWebRequest.Create(_FtpUrl + dir.Name);
        request.Credentials = new NetworkCredential("ftpuser", "KHR3ENJjMh");
        request.Method = WebRequestMethods.Ftp.MakeDirectory;
        try
        {
            request.GetResponse().Close();
        }
        catch { }

        foreach (var file in dir.GetFiles())
        {
            System.Diagnostics.Debug.WriteLine("Uploading: " + file.FullName);
            _FtpServer.UploadFile(_FtpUrl + dir.Name + "/" + file.Name, "STOR", file.FullName);
        }
    }
}